How to get started:

For the moment you should try setting this up on your local computer, we will add instructions on how to do this on the university and desy resources shortly.

Get jupyter: https://jupyter.org/install.html
We recommend the usage of anaconda: https://docs.anaconda.com/anaconda/install/

The folder currently contains 4 small cheat sheets/ tutorials for the most common use cases you will encounter using python.

The first one "IPython_and_Jupyter.ipynb" introduces you into the usage of interactive python and jupyter notebooks. Use this to get started. To open it, start a jupyter server, navigate to the notebook and click on it.

The next one "Intro_Numpy.ipynb" is an introduction into numpy. Numpy contains a lot of usefull tools to handle data structures like arrays but also provides other usefull tools like random numbers.

Somewhat more advanced data structures, so called "frames" can be realized using pandas and are discussed in: "Intro_Pandas.ipynb"

Finally to visualize things you can use matplotlib (In particle physics we usually use another tool called ROOT but for the moment matplotlib is more than enougth and way more userfriendly), see "Matplotlib_Cheatsheet.ipynb" for examples.

After these basic instruction in python tools we recommend to continue with: 
reading this: https://cs231n.github.io/
and trying these: https://pytorch.org/tutorials/

More information will shortly appear here: https://gitlab.rrz.uni-hamburg.de/groups/UHH-ML/tutorials/-/wikis/home

